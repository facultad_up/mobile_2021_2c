// 4 forma de crear objetos, a partir de un prototipo

function Gato(nombre, peso, edad) {
  this.nombre = nombre;
  this.peso = peso;
  this.edad = edad;

  this.saltar = () => {
    console.log('saltando ' + this.nombre);
  };
}

Gato.prototype.maullar = function() {
  console.log('Miauuu soy ' + this.nombre);
};

console.log(Gato.prototype);
let g = new Gato('Pepe', 5.3, 2);
console.log(g);

g.maullar();
g.saltar();

let p = new Gato('Silvestre', 8, 5);
console.log(p);