console.log('Antes');

let funcionPrometida = (resolve, reject) => {
  // la funcion que se ejecuta asincronicamente
  try {
    setTimeout(() => {
      console.log('Se acabo el tiempo');
      resolve('Desde promesa');
    }, 3000);
  } catch {
    reject();
  }
};

let miPromesa = new Promise(funcionPrometida);
miPromesa
  .then(res => {
    console.log('Se resolvio bien: ' + res);
  })
  .catch(() => {
    console.log('Se rompio');
  });

console.log('Despues');
