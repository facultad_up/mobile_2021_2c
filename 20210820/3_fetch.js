// httpbin.org

const url = 'https://swapi.dev/api/people/1';

console.log('Pidiendo datos....');
fetch(url)
  .then(response => {
    console.log(response);
    return response.json();
  })
  .then(data => {
    console.log(data);
  })
  .catch(er => {
    console.error(er);
  });

console.log('Fin.');

// Axios