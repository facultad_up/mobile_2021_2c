const url = 'https://swapi.dev/api/people/1';

async function main() {
  console.log('Comienzo.');

  // async await

  async function consultar() {
    console.log('Paso 2');
    let response = await fetch(url);
    console.log('Paso 3');
    let data = await response.json();
    return data;
  }

  async function realizarConsulta() {
    console.log('Paso 1');
    let data = await consultar();
    console.log(data);
    console.log('Paso 4');
  }

  await realizarConsulta();

  console.log('Fin');
}

main();
