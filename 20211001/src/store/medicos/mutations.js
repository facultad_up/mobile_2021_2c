import { AGREGARMEDICO, EDITARMEDICO, ELIMINARMEDICO } from './types'

export default {
  [AGREGARMEDICO] (state, data) {
    state.medicos.push(data)
  },
  [EDITARMEDICO] (state, data) {
    state.medicos[data.id] = data
  },
  [ELIMINARMEDICO] (state, id) {
    delete state.medicos[id]
  }
}

/*
 datos = {
   id: 393939,
   medico: {
      nombre: 'Juan',
      apellido: 'Perez',
      direccion: 'Rivadavia 4000',
      email: 'josesuarez02@gmail.com',
      fechaNacimiento: '2020/10/02',
      especialidad: 'Oftamología',
   }
 }
 this.$store.commit(AGREGARMEDICO, datos);
*/
