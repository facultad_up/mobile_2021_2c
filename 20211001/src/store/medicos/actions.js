import { AGREGARMEDICO, EDITARMEDICO, ELIMINARMEDICO } from './types'

export default {
  [AGREGARMEDICO]: ({ commit }, medico) => {
    const nuevoMedico = {
      id: medico.dni,
      medico
    }
    commit(AGREGARMEDICO, nuevoMedico)
  },
  [EDITARMEDICO]: ({ commit }, medico) => {
    commit(EDITARMEDICO, medico)
  },
  [ELIMINARMEDICO]: ({ commit }, medicoId) => {
    commit(ELIMINARMEDICO, medicoId)
  }
}
