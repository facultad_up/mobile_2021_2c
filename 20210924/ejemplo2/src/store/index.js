import { store } from 'quasar/wrappers';
import { createStore } from 'vuex';

import medicos from './medicos';
// import pacientes from './pacientes';
// import turnos from './turnos';

export default store((/* { ssrContext } */) => {
  const Store = createStore({
    modules: {
      medicos,
      // pacientes,
      // turnos,
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING,
  });

  return Store;
});
