import Vue from 'vue';
// import { AGREGARMEDICO, EDITARMEDICO, ELIMINARMEDICO } from './types';

export function agregarMedico(state, data) {
  Vue.set(state.medicos, data.id, data.medico);
}

//   [EDITARMEDICO]: (state, data) => {
//     Object.assign(state.medicos[data.id], data.medico);
//   },
//   [ELIMINARMEDICO]: (state, id) => {
//     Vue.delete(state.medicos, id);
//   },
// };
/*
 datos = {
   id: 393939,
   medico: {
      nombre: 'Juan',
      apellido: 'Perez',
      direccion: 'Rivadavia 4000',
      email: 'josesuarez02@gmail.com',
      fechaNacimiento: '2020/10/02',
      especialidad: 'Oftamología',
   }
 }
 this.$store.commit(AGREGARMEDICO, datos);
*/
