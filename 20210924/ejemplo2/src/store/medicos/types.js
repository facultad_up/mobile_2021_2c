const AGREGARMEDICO = 'agregarMedico';
const EDITARMEDICO = 'editarMedico';
const ELIMINARMEDICO = 'eliminarMedico';

export {
  AGREGARMEDICO,
  EDITARMEDICO,
  ELIMINARMEDICO,
};
