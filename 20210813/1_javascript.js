
console.log('Hola mundo!') // comentario en linea

/*
 * Comentario
 * Multilinea
 */

// nomenclatura: camelCase

const pi = 3.14

var x = 1
let y = 1

var x = "abc"
console.log(x)

// let y = 2 // No se puede hacer

function funcion1 (a, b) {
  console.log(a+b)
}

funcion1(3,4)

let funcion2 = function (a, b) {
  console.log('Otra funcion.')
  console.log(a*b)
}

funcion2(54,2)

console.log(typeof funcion2)
console.log(funcion2.toString())

let funcion3 = funcion2
funcion3(1,9)

let funcion4 = (i,j) => {
  console.log("Soy arrow")
  return i-j
}

// Closures
console.log('--------------------------------')

function saludar() {
  var nombre = 'Gabriel'

  function decirHola() {
    console.log(nombre)    
  }

  return decirHola
}

let s = saludar()
s()

console.log('--------------------------------')

function crearContador(inicio, salto) {
  function siguiente() {
    return inicio += salto
  }

  return siguiente
}

let c1 = crearContador(5,2)
c1()
console.log(c1())
console.log(c1())

console.log('--------------------------------')

function crearContadorPulenta() {
  let contador = 0
  function setearContador(valor) {
    contador += valor
  }
  return {
    incrementar: function() {
      setearContador(1)
    },
    decrementar: function() {
      setearContador(-1)
    },
    valor: function() {
      return contador
    }
  }
}

let c2 = crearContadorPulenta()
c2.incrementar()
c2.incrementar()
console.log(c2.valor())

