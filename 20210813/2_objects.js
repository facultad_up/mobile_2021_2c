// Forma 1 de crear objetos

let o1 = new Object();
o1.nombre = 'Gabriel';
o1.altura = 175;
o1.saludar = function() {
  console.log(this.nombre);
};

console.log(o1);
o1.saludar();

// Forma 2

let o2 = {
  nombre: 'Gabriel',
  altura: 175,
  saludar: function() {
    console.log(this.nombre);
  }
};

console.log(o2);
o2.saludar();

// Forma 3

class Persona {
  constructor(nombre, altura) {
    this.nombre = nombre
    this.altura = altura
  }

  saludar() {
    console.log(this.nombre);
  }
}

let o3 = new Persona('Gabriel', 175)
o3.saludar()

// JSON: JavaScript Object Notation   ""
let s1 = JSON.stringify(o3)
console.log(s1)
console.log(typeof s1)

let s2 = '{"nombre":"Gabriel","altura":175}'

let o4 = JSON.parse(s1)
console.log(o4)

let o5 = Object.setPrototypeOf(o4, Persona.prototype)
console.log(o5)
o5.saludar()
